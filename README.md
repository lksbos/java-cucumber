#Checkout

- This is a cucumber java project with education purposes.

##Build

- To build the project run "mvn clean package" on the root of the project.

##Test

- To test the project run "mvn test" on the root of the project to see the tests results.

###Check the tests results

- You can check on the mvn console or open the "index.html" file on "target/features" path.
