Feature: Checkout
    In order to make a checkout
    As a cashier
    I want to register each product as a checkout item 
    increasing the total value of the shopping for each one.

    Background:
        Given the price of a "banana" is 40c
        And the price of a "apple" is 25c
    
    Scenario Outline: Checkout bananas
        When I checkout <quantity> "banana"
        Then the total price should be <total>c
        And the checkout items should be 1
        
        Examples:
        | quantity | total|
        | 1     | 40   |
        | 2     | 80   |
        | 4     | 160   |

    Scenario: Checkout two bananas scanned separately
            When I checkout 1 "banana"
            And I checkout 1 "banana"
            Then the total price should be 80c
            And the checkout items should be 2
    
    Scenario: Checkout a banana and an apple
            When I checkout 1 "banana"
            And I checkout 1 "apple"
            Then the total price should be 65c
            And the checkout items should be 2