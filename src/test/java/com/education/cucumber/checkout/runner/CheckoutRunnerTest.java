/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.education.cucumber.checkout.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 *
 * @author lucas
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"progress", "html:target/features"},
        snippets = SnippetType.CAMELCASE,
        features = "classpath:features/checkout.feature",
        glue = {"com.education.cucumber.checkout.steps"}
)
public class CheckoutRunnerTest {

}
