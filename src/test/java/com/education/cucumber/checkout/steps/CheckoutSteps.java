/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.education.cucumber.checkout.steps;

import com.education.cucumber.checkout.entity.Product;
import com.education.cucumber.checkout.entity.Checkout;
import cucumber.api.java.Before;
import cucumber.api.java.en.*;
import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.*;

/**
 *
 * @author lucas
 */
public class CheckoutSteps {
    private Checkout checkout;
    private Map<String, Product> products;
    
    @Before
    public void setup(){
        products = new HashMap<String, Product>();
        checkout = new Checkout();
    }

    @Given("^the price of a \"([^\"]*)\" is (\\d+)c$")
    public void thePriceOfAIsC(String name, int price) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        products.put(name, new Product(name, price));
    }
    

    @When("^I checkout (\\d+) \"([^\"]*)\"$")
    public void iCheckout(int itemCount, String name) throws Throwable {
       checkout.add(itemCount, products.get(name));
    }
    
    @Then("^the total price should be (\\d+)c$")
    public void theTotalPriceShouldBeC(int total) throws Throwable {
        assertEquals(total, checkout.getTotal());
    }
    
    @Then("^the checkout items should be (\\d+)$")
    public void theProductListSizeShouldBe(int quantity) throws Throwable {
        assertEquals(quantity, checkout.getProducts().size());
    }

}
