/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.education.cucumber.checkout.entity;

/**
 *
 * @author lucas
 */
public class CheckoutItem {

    private int count;
    private Product product;
    private Checkout checkout;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Checkout getCheckout() {
        return checkout;
    }

    public void setCheckout(Checkout checkout) {
        this.checkout = checkout;
    }

    public CheckoutItem(int count, Product product, Checkout checkout) {
        this.count = count;
        this.product = product;
        this.checkout = checkout;
    }

    public CheckoutItem() {

    }
    
    public int getTotalPrice() {
        return (count*product.getPrice());
    }
}
