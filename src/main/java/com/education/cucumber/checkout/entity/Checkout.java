package com.education.cucumber.checkout.entity;

import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author lucas
 */
public class Checkout {
    private int runningTotal = 0;
    private List<CheckoutItem> items = new ArrayList<CheckoutItem>();
    
    public void add(int count, Product product) {
        CheckoutItem item = new CheckoutItem(count, product, this);
        items.add(item);
        runningTotal += item.getTotalPrice();
    }

   

    public int getTotal() {
        return runningTotal;
    }

    public List<CheckoutItem> getProducts() {
        return items;
    }

}
